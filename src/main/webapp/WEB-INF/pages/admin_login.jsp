<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head> 
	<meta charset="utf-8" /> 
	<title>Admin Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 

	<link rel="stylesheet" href="css/app.v2.css" type="text/css" /> 

	<link rel="stylesheet" href="css/font.css" type="text/css" cache="false" /> 
<!--[if lt IE 9]> 
<script src="js/ie/respond.min.js" cache="false">
</script> 
<script src="js/ie/html5.js" cache="false">
</script> 
<script src="js/ie/fix.js" cache="false">
</script> <![endif]-->
</head>

<body> <section id="content" class="m-t-lg wrapper-md animated fadeInUp"> <a class="nav-brand" href="/">Administration</a> 

	<div class="row m-n"> 

		<div class="col-md-4 col-md-offset-4 m-t-lg"> <section class="panel"> 

			<form action="/admin" class="panel-body" method="post"> 

				<div class="form-group"> 
					<label class="control-label">Login</label>
					<input type="text" class="form-control" name="login">
				</div> 

				<div class="form-group"> 
					<label class="control-label">Password</label>
					<input type="password" name="password" id="inputPassword" class="form-control"> 
				</div> 

				<div class="pull-right"><button type="submit" class="btn btn-info">Sign in</button></div>
			</form> </section> 
		</div> 
	</div> </section> 
	<!-- footer --> <footer id="footer"> 

	<div class="text-center padder clearfix"> 

		<p> <small>&copy; JWeb - 2014</small> </p> 
		</div> </footer> 
		<!-- / footer -->
		<script src="js/app.v2.js">
		</script> 
		<!-- Bootstrap --> 
		<!-- app --> 
	</body>
	</html>