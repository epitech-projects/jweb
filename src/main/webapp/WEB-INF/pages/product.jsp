<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>${product.displayName}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/style.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.8.3.min.js"><\/script>')</script>
    <script src="../js/html5.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/radio.js"></script>
    <script src="../js/checkbox.js"></script>
    <script src="../js/selectBox.js"></script>
    <script src="../js/jquery.carouFredSel-6.2.0-packed.js"></script>
    <script src="../js/jquery.touchSwipe.min.js"></script>
    <script src="../js/jquery.jqzoom-core.js"></script>
    <script src="../js/jquery.transit.js"></script>
    <script src="../js/jquery.easing.1.2.js"></script>
    <script src="../js/jquery.anythingslider.js"></script>
    <script src="../js/jquery.anythingslider.fx.js"></script>
</head>
<body>
    <jsp:include page="header.jsp" />
    <section id="main">
        <div class="container_12">
            <div id="content" class="grid_12">
                <header>
                    <h1 class="page_title">${product.displayName}</h1>
                </header>

                <article class="product_page negative-grid">
                    <div class="grid_5 img_slid" id="products">
                        <div class="preview slides_container">
                            <div class="prev_bg">
                                <a href="../img/watches/${product.name}/product.png" class="jqzoom" rel='gal1' title="">
                                    <img src="../img/watches/${product.name}/product.png" alt="Product 1" title="" style="width: 100%">
                                </a>
                            </div>
                        </div><!-- .preview -->
                    </div><!-- .grid_5 -->

                    <div class="grid_7">
                        <div class="entry_content">
                        	${product.description}
                            &nbsp;
                            <div class="ava_price">
                                <div class="price product-price">
									$ ${product.price}
                                </div><!-- .price -->
                                <div class="clear"></div>
                            </div><!-- .ava_price -->

                            <div class="cart">
                                <c:choose>
                                    <c:when test="${empty user}">
                                        <a href="/login" class="bay"><img src="../img/bg_cart.png" alt="Buy" title="">Add to Cart</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="bay add-to-cart"><img src="../img/bg_cart.png" alt="Buy" title="">Add to Cart</a>
                                    </c:otherwise>
                                </c:choose>
                            </div><!-- .cart -->
                        </div><!-- .entry_content -->
                    </div><!-- .grid_7 -->
                    <div class="clear"></div>
                </article><!-- .product_page -->
                <div class="clear"></div>
            </div><!-- #content -->
            <div class="clear"></div>
        </div><!-- .container_12 -->
    </section><!-- #main -->
    <div class="clear"></div>
    <jsp:include page="footer.jsp" />
</body>

<script>
    $(document).ready(function() {
        var index = 0;

        var imgurl = $('.jqzoom').attr('href');
        var displayName = $('.page_title').text();
        var price = $('.product-price').text();

        $('.add-to-cart').click(function() {
            $.ajax({
                type: 'post',
                url: '/cart/add',
                success: function(data) {
                    if (data == "true") {
                        $('#cart_nav').effect("bounce", "slow");

                        $('<li class="product-li product-' + index + '"><a href="' + window.location.pathname + '" class="prev_cart"><div class="cart_vert"><img src="' + imgurl + '" alt="' + displayName + '" title=""></div></a><div class="cont_cart"><h4>' + displayName + '</h4><div class="price"><span>' + price + '</span></div></div><a title="close" class="close delete-product" index="' + index + '" href="#"></a><div class="clear"></div></li>').insertBefore('.insertBefore');

                        index++;
                    }
                },
                data: { 'id': ${product.id} }
            });
        });
    });
</script>

</html>