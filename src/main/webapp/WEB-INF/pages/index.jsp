<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" href="../favicon.ico">

    <link rel="stylesheet" href="../css/style.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.8.3.min.js"><\/script>')</script>
    <script src="../js/html5.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/radio.js"></script>
    <script src="../js/checkbox.js"></script>
    <script src="../js/selectBox.js"></script>
    <script src="../js/jquery.carouFredSel-6.2.0-packed.js"></script>
    <script src="../js/jquery.touchSwipe.min.js"></script>
    <script src="../js/jquery.jqzoom-core.js"></script>
    <script src="../js/jquery.transit.js"></script>
    <script src="../js/jquery.easing.1.2.js"></script>
    <script src="../js/jquery.anythingslider.js"></script>
    <script src="../js/jquery.anythingslider.fx.js"></script>
</head>
<body>
    <jsp:include page="header.jsp" />
    <div id="slider_body">
        <ul id="slider">
            <li>
                <div class="slid_content">
                    <h2 style="color:#16171B;">Flemish Baroque</h2>
                    <p></p>
                    <a class="buy_now" href="/product/flemish-baroque">Buy now</a>
                </div><!-- .slid_content -->
                <img src="../img/watches/flemish-baroque/slide.jpg" alt="Slid 1" title="">
            </li>

            <li>
                <div class="slid_content">
                    <h2 style="color:#E8E2D5;">Denim Polkadot</h2>
                    <p></p>
                    <a class="buy_now" href="/product/denim-polkadot">Buy now</a>
                </div><!-- .slid_content -->
                <img src="../img/watches/denim-polkadot/slide.jpg" alt="Slid 2" title="">
            </li>

            <li>
                <div class="slid_content">
                    <h2 style="color:#000;">Expressionist</h2>
                    <p></p>
                    <a class="buy_now" href="/product/expressionist">Buy now</a>
                </div><!-- .slid_content -->
                <img src="../img/watches/expressionist/slide.jpg" alt="Slid 3" title="">
            </li>
        </ul>
    </div><!-- #slider_body -->

    <div id="home_banners">
        <div class="container_12">
            <div class="grid_3">&nbsp;</div>
            <div style="margin-left: 60px;" class="grid_6">
                <a href="#" class="banner banner1"><strong>Free shipping</strong> on orders over $50</a>
            </div><!-- .grid_6 -->
            <div class="grid_3">&nbsp;</div>

            <!-- <div class="grid_6">
                <a href="#" class="banner banner2"><strong>Insane discounts</strong> on new collection!</a>
            </div> --><!-- .grid_6 -->
            <div class="clear"></div>
        </div>
    </div><!-- #home_banners -->
    <jsp:include page="footer.jsp" />
</body>
</html>
