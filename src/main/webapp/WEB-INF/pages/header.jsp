<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="top">
    <div class="container_12">
        <div class="grid_3">
            <div class="lang">
                <ul>
                    <li><a href="/">Home</a></li>
                </ul>
            </div><!-- .home -->
        </div><!-- .grid_3 -->
        <div class="grid_9">
            <nav>
                <ul>
                    <c:choose>
                        <c:when test="${not empty user}">
                            <li><a href="#">${user.mail}</a></li>
                            <li><a href="/logout">Logout</li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="/login">Log In</a></li>
                            <li><a href="/register">Sign Up</a></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </nav>
        </div><!-- .grid_9 -->
    </div>
</div><!-- #top -->

<header id="branding">
    <div class="container_12">
        <div class="grid_3">
            <hgroup>
                <h1 id="site_logo"><a href="/" title="Komono"><img src="../img/logo-komono.svg" alt="Online Komono Store"></a></h1>
            </hgroup>
        </div><!-- .grid_3 -->

        <div class="grid_9">
            <div class="top_header">
                    <c:if test="${empty user}">
                    <div class="welcome dropdown">
                        Welcome visitor you can <a href="/login">login</a> or <a href="/register">create an account</a>.
                    </div><!-- .welcome -->
                </c:if>

                <c:if test="${not empty user}">
                    <ul id="cart_nav">
                        <li>
                            <a class="cart_li" href="#">
                                <div class="cart_ico"></div>
                                Cart
                                <span>&nbsp;</span>
                            </a>
                            <ul class="cart_cont">
                                <li class="no_border recently">Recently added item(s)</li>
                                <c:forEach var="product" items="${cart}" varStatus="status">
                                    <li class="product-li product-${status.index}">
                                        <a href="/product/${product.name}" class="prev_cart"><div class="cart_vert"><img src="../img/watches/${product.name}/product.png" alt="${product.displayName}" title=""></div></a>
                                        <div class="cont_cart">
                                            <h4>${product.displayName}</h4>
                                            <div class="price"><span>$ ${product.price}</span></div>
                                        </div>
                                        <a title="close" class="close delete-product" index="${status.index}" href="#"></a>
                                        <div class="clear"></div>
                                    </li>
                                </c:forEach>
                                <li class="no_border insertBefore">
                                    <a style="margin-left: 170px;" href="#" class="pay-product checkout">Proceed to Checkout</a>
                                </li>
                            </ul>
                        </li>
                    </ul><!-- .cart_nav -->
                </c:if>
            </div><!-- .top_header -->
        </div><!-- .grid_9 -->
    </div>
    <div class="clear"></div>
</header>

    <script>
    $(document).ready(function(){
        $(document).on('click', '.delete-product', function() {
            var id = $(this).attr('index');

            $.ajax({
                type: 'post',
                url: '/cart/remove',
                success: function() {
                    $('.product-' + id).fadeOut();
                },
                data: { 'id': id }
            });
        });

        $(document).on('click', '.pay-product', function() {
            $.ajax({
                type: 'post',
                url: '/cart/pay',
                success: function() {
                    $('.product-li').fadeOut();
                },
                data: { 'id': 0 }
            });
        });
    });
    </script>