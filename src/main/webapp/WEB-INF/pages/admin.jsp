<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head> 
	<meta charset="utf-8" /> 
	<title>Administration Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 

	<link rel="stylesheet" href="css/app.v2.css" type="text/css" />

	<link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
<!--[if lt IE 9]> 
<script src="js/ie/respond.min.js" cache="false">
</script> 
<script src="js/ie/html5.js" cache="false">
</script> 
<script src="js/ie/fix.js" cache="false">
</script> <![endif]-->
</head>

<body> 
	<section id="content">
		<section class="hbox stretch">
			<aside class="aside bg-white b-r" id="subNav"> 

				<section class="scrollable wrapper w-f"> <section class="panel"> 

					<div class="table-responsive"> 
						<table class="table table-striped m-b-none"> 
							<thead> 
								<tr> 
									<th width="20">ID</th>
									<th class="th-sortable" data-toggle="class">User ID
										<span class="th-sort"> <i class="fa fa-sort-down text"></i> <i class="fa fa-sort-up text-active"></i> <i class="fa fa-sort"></i></span>
									</th> 
									<th>Product(s)</th>
									<th>Date</th>
									<!-- <th width="30"></th> -->
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orders}" var="order"> <!-- forEach -->
									<tr>
										<td>${order.id}</td>
										<td>${order.userId}</td>
										<td>${order.productList}</td>
										<td>${order.orderDate}</td>
									</tr>
								</c:forEach> <!-- /forEach -->
							</tbody> 
						</table> 
					</div>
				</section>
			</section>

			<footer class="footer bg-white b-t"> 
				<div class="row m-t-sm text-center-xs"> 
					<div class="col-sm-12 text-center">
						<small class="text-muted inline m-t-sm m-b-sm">showing ${size} of ${size} item(s)</small>
					</div> 
				</div>
			</footer>
		</aside>
	</section>
</section>
<a href="master.html#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> </section> 
<!-- /.vbox --> </section> 

<script src="js/app.v2.js">
</script> 
<!-- Bootstrap --> 
<!-- App --> 
</body>
</html>