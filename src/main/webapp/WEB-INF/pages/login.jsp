<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    
    <link rel="shortcut icon" href="../favicon.ico">

    <link rel="stylesheet" href="css/style.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.3.min.js"><\/script>')</script>
    <script src="js/html5.js"></script>
    <script src="js/main.js"></script>
    <script src="js/radio.js"></script>
    <script src="js/checkbox.js"></script>
    <script src="js/selectBox.js"></script>
    <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
    <script src="js/jquery.touchSwipe.min.js"></script>
    <script src="js/jquery.jqzoom-core.js"></script>
    <script src="js/jquery.transit.js"></script>
    <script src="js/jquery.easing.1.2.js"></script>
    <script src="js/jquery.anythingslider.js"></script>
    <script src="js/jquery.anythingslider.fx.js"></script>
</head>
<body>
	<jsp:include page="header.jsp" />
    <section id="main" class="page-login">
        <div class="container_12">
            <div id="content" class="grid_12">
                <header>
                    <h1 class="page_title">Login or Create an Account</h1>
                    <c:if test="${error}">
                        <h1 style="color: red;">ERROR</h1>
                    </c:if>
                </header>

                <article>
                    <div class="grid_6 new_customers">
                       <h2>New Customers</h2>
                       <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                       <div class="clear"></div>
                       <button class="account" onclick="window.location.href='/register'">Create An Account</button>
                   </div><!-- .grid_6 -->

                   <div class="grid_6 registed_form">
                       <form class="registed" action="login" method="post">
                           <h2>Registed Customers</h2>
                           <p>If you have an account with us, please log in.</p>
                           <div class="email">
                            <strong>Email Adress:</strong><sup>*</sup><br>
                            <input type="email" name="email" class="" value="">
                        </div><!-- .email -->
                        <div class="password">
                            <strong>Password:</strong><sup>*</sup><br>
                            <input type="password" name="password" class="" value="">
                        </div><!-- .password -->
                        <div class="submit">
                            <input type="submit" value="Login">
                            <span>* Required Field</span>
                            <div class="clear"></div>
                        </div><!-- .submit -->
                    </form><!-- .registed -->
                </div><!-- .grid_6 -->
            </article>

            <div class="clear"></div>
        </div><!-- #content -->

        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<div class="clear"></div>
<jsp:include page="footer.jsp" />
</body>
</html>
