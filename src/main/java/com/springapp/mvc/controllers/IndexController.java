package com.springapp.mvc.controllers;

import com.springapp.mvc.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/")

public class IndexController {

	@Autowired UserDao userDao;

	@RequestMapping(method = RequestMethod.GET)
	public String displayView() {
		return "index";
	}
}