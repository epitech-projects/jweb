package com.springapp.mvc.controllers;

import com.springapp.mvc.models.Product;
import com.springapp.mvc.models.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/product/{productName}")

public class ProductController {

    @Autowired
    ProductDao productDao;

    @RequestMapping(method = RequestMethod.GET)
    public String DisplayProduct(Model model, @PathVariable String productName) {
        Product product = productDao.findProductByName(productName);
        model.addAttribute("product", product);

        return "product";
    }
}
