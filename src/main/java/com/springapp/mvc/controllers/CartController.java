package com.springapp.mvc.controllers;

import com.springapp.mvc.models.UserOrderDao;
import com.springapp.mvc.models.Product;
import com.springapp.mvc.models.ProductDao;
import com.springapp.mvc.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

@Controller
@RequestMapping("/cart/{action}")

public class CartController {

    @Autowired
    UserOrderDao userOrderDao;

    @Autowired
    ProductDao productDao;

    ArrayList<Product> cart;

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String doAction(Model model, @PathVariable String action, @RequestParam(value = "id", required = true) Long id, HttpSession session) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        if (action.equals("remove")) {
            if (!removeProduct(id))
                return "false remove";
            return "true";
        }
        else if (action.equals("pay"))
            if (!payProduct(session))
                return "false pay";

        Product product = productDao.findProductById(id);
        cart = (ArrayList<Product>) session.getAttribute("cart");

        if (product == null)
            return "product is null";

        if (action.equals("add"))
            addProduct(product);

        session.setAttribute("cart", cart);
        model.addAttribute("cart", cart);

        return "true";
    }

    private Boolean addProduct(Product product) {
        if (cart == null)
            cart = new ArrayList<Product>();

        cart.add(product);
        return true;
    }

    private Boolean removeProduct(Long index) {
        if (cart == null || (cart.size() < index))
            return false;

        cart.remove(index.intValue());
        return true;
    }

    private Boolean payProduct(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null)
            return false;
        if (cart.size() <= 0)
            return false;

        userOrderDao.addOrder(user.getId(), cart);
        cart.clear();
        return true;
    }
}
