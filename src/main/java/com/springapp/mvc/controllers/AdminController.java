package com.springapp.mvc.controllers;


import com.springapp.mvc.models.Admin;
import com.springapp.mvc.models.AdminDao;
import com.springapp.mvc.models.UserOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/admin")

public class AdminController {

    @Autowired AdminDao adminDao;
    @Autowired UserOrderDao userOrderDao;

    @RequestMapping(method = RequestMethod.GET)
    public String displayView(Model model, HttpSession session) {

        Admin admin = (Admin) session.getAttribute("admin");
        if (admin != null) {
            List orders = userOrderDao.findAll();

            model.addAttribute("orders", orders);
            model.addAttribute("size", orders.size());
            return "admin";
        }
        return "admin_login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String login(Model model,
                        HttpSession session,
                        @RequestParam(value = "login", required = true) String login,
                        @RequestParam(value = "password", required = true) String password) {
        Admin admin = adminDao.findAdminByLogin(login);
        if (admin == null || !DigestUtils.md5DigestAsHex(password.getBytes()).equals(admin.getPassword())) {
            model.addAttribute("error", true);
            return "admin_login";
        }

        session.setAttribute("admin", admin);
        return "redirect:/admin";
    }
}
