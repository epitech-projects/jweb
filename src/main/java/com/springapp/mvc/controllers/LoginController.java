package com.springapp.mvc.controllers;

import com.springapp.mvc.models.User;
import com.springapp.mvc.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/login")

public class LoginController {

    @Autowired
    UserDao userDao;

    @RequestMapping(method = RequestMethod.GET)
    public String displayView(HttpSession session) {

        User user = (User) session.getAttribute("user");
        if (user != null) {
            return "redirect:/";
        }

        return "login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String login(Model model,
                        HttpSession session,
                        @RequestParam(value = "email", required = true) String email,
                        @RequestParam(value = "password", required = true) String password) {
        User user = userDao.findUserByCriteria("mail", email);
        if (user == null || !DigestUtils.md5DigestAsHex(password.getBytes()).equals(user.getPassword())) {
            model.addAttribute("error", true);
            return "login";
        }
        session.setAttribute("user", user);
        return "redirect:/";
    }

}