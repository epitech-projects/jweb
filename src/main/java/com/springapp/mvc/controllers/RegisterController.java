package com.springapp.mvc.controllers;

import com.springapp.mvc.models.User;
import com.springapp.mvc.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/register")

public class RegisterController {

	@Autowired UserDao userDao;

	@RequestMapping(method = RequestMethod.GET)
	public String displayView(Model model, HttpSession session) {

		User user = (User) session.getAttribute("user");
		if (user != null) {
			return "redirect:/";
		}

		return "register";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String register(Model model, HttpSession session,
		@RequestParam(value = "email", required = true) String email,
		@RequestParam(value = "password", required = true) String password,
		@RequestParam(value = "password-check", required = true) String passwordCheck) {

		if (!password.equals(passwordCheck)) {
			model.addAttribute("error", true);
			return "register";
		}

		User user = userDao.addUser(email, password, false);
		session.setAttribute("user", user);
		model.addAttribute("user", user);

		return "redirect:/";
	}

}