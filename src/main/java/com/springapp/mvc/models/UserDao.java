package com.springapp.mvc.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@SuppressWarnings({"unchecked", "rawtypes"})

public class UserDao {
  @Autowired private SessionFactory sessionFactory;

  @Transactional
  public List<User> findAll() {
    Session session = sessionFactory.getCurrentSession();
      return session.createQuery("from User").list();
  }

  @Transactional
  public User findUserByCriteria(String criteria, String value) {
    Session session = sessionFactory.getCurrentSession();
    return (User) session.createCriteria(User.class).add(Restrictions.like(criteria, value)).uniqueResult();
  }

  @Transactional
  public User findUserById(Integer id) {
    Session session = sessionFactory.getCurrentSession();
    return (User) session.createCriteria(User.class).add(Restrictions.eq("id", id)).uniqueResult();
  }

  @Transactional
  public User addUser(String mail,
                      String password,
                      Boolean type) {
    Session session = sessionFactory.getCurrentSession();

    User user = new User();

    user.setMail(mail);
    user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
    user.setSubscriber(false);
    user.setType(type);

    session.save(user);
    session.flush();

    return user;
  }
}