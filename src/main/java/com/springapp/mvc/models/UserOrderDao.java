package com.springapp.mvc.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository

public class UserOrderDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from UserOrder").list();
    }

    @Transactional
    public void addOrder(Long userId, ArrayList<Product> productList) {
        Session session = sessionFactory.getCurrentSession();
        String stringList = "";

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date today = Calendar.getInstance().getTime();

        for (Product product : productList) {
            stringList += product.getName() + "<br/>";
        }

        UserOrder order = new UserOrder();
        order.setUserId(userId);
        order.setProductList(stringList);
        order.setOrderDate(dateFormat.format(today));

        session.save(order);
        session.flush();
    }
}
