package com.springapp.mvc.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository

public class ProductDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Product").list();
    }

    @Transactional
    public Product findProductById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (Product) session.createCriteria(Product.class).add(Restrictions.eq("id", id)).uniqueResult();
    }

    @Transactional
    public Product findProductByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        return (Product) session.createCriteria(Product.class).add(Restrictions.like("name", name)).uniqueResult();
    }

    @Transactional
    public void addProduct(String name,
                           String displayName,
                           String description,
                           Double price,
                           Integer stock) {
        Session session = sessionFactory.getCurrentSession();

        Product product = new Product();

        product.setName(name);
        product.setDescription(description);
        product.setDisplayName(displayName);
        product.setPrice(price);
        product.setStock(stock);

        session.save(product);
        session.flush();
    }
}
