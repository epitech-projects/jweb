package com.springapp.mvc.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@SuppressWarnings({"unchecked", "rawtypes"})

public class AdminDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public Admin findAdminByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        return (Admin) session.createCriteria(Admin.class).add(Restrictions.eq("login", login)).uniqueResult();
    }
}
