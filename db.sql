-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 27, 2014 at 11:54 AM
-- Server version: 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `JWeb`
--

-- --------------------------------------------------------

--
-- Table structure for table `UserOrder`
--

CREATE TABLE `UserOrder` (
`id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productList` text NOT NULL,
  `orderDate` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `UserOrder`
--

INSERT INTO `UserOrder` (`id`, `userId`, `productList`, `orderDate`) VALUES
(1, 0, '[''TEST'', ''TEST2'']', '0000-00-00 00:00:00'),
(2, 23, '[com.springapp.mvc.models.Product@7f7f901c]', '0000-00-00 00:00:00'),
(3, 23, 'flemish-baroque\nflemish-baroque\nflemish-baroque\nflemish-baroque\n', '0000-00-00 00:00:00'),
(4, 23, 'flemish-baroque<br/>', '27/12/2014 04:23'),
(5, 23, 'flemish-baroque<br/>flemish-baroque<br/>flemish-baroque<br/>', '27/12/2014 04:24'),
(6, 23, 'flemish-baroque<br/>', '27/12/2014 04:42'),
(7, 23, 'flemish-baroque<br/>flemish-baroque<br/>', '27/12/2014 04:43'),
(8, 23, 'flemish-baroque<br/>flemish-baroque<br/>', '27/12/2014 04:43'),
(9, 23, 'flemish-baroque<br/>flemish-baroque<br/>', '27/12/2014 04:44'),
(10, 23, '', '27/12/2014 04:44'),
(11, 23, 'flemish-baroque<br/>', '27/12/2014 04:44'),
(12, 23, 'flemish-baroque<br/>', '27/12/2014 04:44'),
(13, 23, 'flemish-baroque<br/>', '27/12/2014 04:45'),
(14, 23, 'flemish-baroque<br/>denim-polkadot<br/>', '27/12/2014 04:46'),
(15, 23, '', '27/12/2014 04:47'),
(16, 23, '', '27/12/2014 04:47'),
(17, 23, '', '27/12/2014 04:47'),
(18, 23, '', '27/12/2014 04:47'),
(19, 23, '', '27/12/2014 04:47'),
(20, 23, '', '27/12/2014 04:47'),
(21, 23, '', '27/12/2014 04:47'),
(22, 23, '', '27/12/2014 04:47'),
(23, 23, '', '27/12/2014 04:47'),
(24, 23, 'flemish-baroque<br/>', '27/12/2014 04:49'),
(25, 23, '', '27/12/2014 04:49'),
(26, 23, '', '27/12/2014 04:49'),
(27, 23, '', '27/12/2014 04:49'),
(28, 23, '', '27/12/2014 04:49'),
(29, 23, '', '27/12/2014 04:49'),
(30, 23, 'flemish-baroque<br/>', '27/12/2014 04:50');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
`id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `login`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `displayName`, `description`, `price`, `stock`) VALUES
(1, 'flemish-baroque', 'Flemish Baroque', 'Une super montre trop top, moi perso j''adore. Une super montre trop top, moi perso j''adore. Une super montre trop top, moi perso j''adore. Une super montre trop top, moi perso j''adore.', 59, 143),
(2, 'denim-polkadot', 'Denim Polkadot', 'Génial celle la, en plus elle donne l''heure c''est tellement cool. Génial celle la, en plus elle donne l''heure c''est tellement cool. Génial celle la, en plus elle donne l''heure c''est tellement cool. ', 69, 56),
(3, 'expressionist', 'Expressionist', 'Une montre digne des plus grand charcutier, un design élégant et ta gueule. Une montre digne des plus grand charcutier, un design élégant et ta gueule. Une montre digne des plus grand charcutier, un design élégant et ta gueule. ', 59, 86);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
`id` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `subscriber` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `mail`, `password`, `subscriber`, `type`) VALUES
(18, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0),
(19, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0),
(20, '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0),
(21, 'aze@aze.com', '0a5b3913cbc9a9092311630e869b4442', 0, 0),
(22, 'azeazezeazezae@azeaegdsg.czef', '721a9b52bfceacc503c056e3b9b93cfa', 0, 0),
(23, 'jean.gravier2@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `UserOrder`
--
ALTER TABLE `UserOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `UserOrder`
--
ALTER TABLE `UserOrder`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
